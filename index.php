<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="CAPA_PRESENTACION/css/bootstrap.min.css">
    <link rel="stylesheet" href="CAPA_PRESENTACION/css/estilo.css">
    <link rel="stylesheet" href="CAPA_PRESENTACION/font-awesome-4.3.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Computer Gv</title>
</head>
<body>
    <header>
        <div class="row">
            <div class="col col-lg-12">
                <nav class="navbar navbar-inverse navbar-static-top" role="navogation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Computer Gv</a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="" class="fa fa-home"> Inicio</a></li>
                                <li><a href="">Catalogo</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                        Productos <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="">Computadoras</a></li>
                                        <li><a href="">Laptops</a></li>
                                        <li><a href="">Impresoras</a></li>
                                        <li class="divider"></li>
                                        <li><a href="">Suministros</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav pull-right">
                                <li><a href="" class="fa fa-user"> Sessión</a></li>
                                <li class="divider-vertical"></li>
                                <li><a href="">Ayuda</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row-fluid banner-principal">
            <div class="carousel slide" id="carousel-principal" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-principal" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-principal" data-slide-to="1"></li>
                    <li data-target="#carousel-principal" data-slide-to="2"></li>
                    <li data-target="#carousel-principal" data-slide-to="3"></li>
                    <li data-target="#carousel-principal" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img class="carousel-img" src="CAPA_PRESENTACION/img/Penguins.jpg" alt="">
                        <div class="carousel-caption">
                            <h3>Penguins</h3>
                        </div>
                    </div>
                    <div class="item">
                        <img class="carousel-img" src="CAPA_PRESENTACION/img/Desert.jpg" alt="">
                        <div class="carousel-caption">
                            <h3>Desert</h3>
                        </div>
                    </div>
                    <div class="item">
                        <img class="carousel-img" src="CAPA_PRESENTACION/img/Tulips.jpg" alt="">
                        <div class="carousel-caption">
                            <h3>Tulips</h3>
                        </div>
                    </div>
                    <div class="item">
                        <img class="carousel-img" src="CAPA_PRESENTACION/img/Koala.jpg" alt="">
                        <div class="carousel-caption">
                            <h3>Koala</h3>
                        </div>
                    </div>
                    <div class="item">
                        <img class="carousel-img" src="CAPA_PRESENTACION/img/Jellyfish.jpg" alt="">
                        <div class="carousel-caption">
                            <h3>Jellyfish</h3>
                        </div>
                    </div>
                </div>
                <a href="#carousel-principal" class="left carousel-control" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a href="#carousel-principal" class="right carousel-control" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Siguiente</span>
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="caja">
            <div class="row-fluid">
                <div class="col col-lg-9 central">
                    <p id="titulo">Ultimos Equipos</p>
                    <div id="body">
                        <img id="mac"src="CAPA_PRESENTACION/img/macbook_air.png">
                        <p id="titulo_uno">El Mac App Store es el medio más sencillo de encontrar y descargar apps para tu Mac.</p>
                        <p id="descripcion">Para descargar Web Design 205: Designing CSS Floating Layouts del Mac App Store, necesitas un Mac con OS X 10.6.6 o posterior.
                            <a class="a-mac" href="#">Más información</a>.
                        </p>
                    </div>
                </div>
                <div class="col col-lg-3 logeo">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h2 id="titulo-logeo">Logeo</h2>
                            <hr>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" placeholder="Usuario">
                            </div>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="text" class="form-control" placeholder="Password">
                            </div>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Ingresar" name="btnIngresar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--comentar-->
    </div>
    <section class="main container">
        <div class="row">
            <section class="posts col-md-9">
                <div class="miga-de-pan">
                    <ol class="breadcrumb">
                        <li><a href="#">Inicio</a></li>
                        <li><a href="#">Categoria</a></li>
                        <li class="active">Diseño web</a></li>
                    </ol>
                </div>
                <article class="post clearfix">
                    <a href="#" class="thumb pull-left">
                        <img class="img-thumbnail" src="CAPA_PRESENTACION/images/img1.jpg" alt="">
                    </a>
                    <h2 class="post-tittle">
                        <a href="#">Inicia proyectos HTML5 mas rápido con Initializar</a>
                    </h2>
                    <p><span class="post-fecha">26 de enero del 2015</span> por <span class="post-autor"><a href="#">Manuel Guarniz</a></span></p>
                    <p class="post-contenido text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                    <div class="contenedor-botones">
                        <a href="#" class="btn btn-primary">Leer más</a>
                        <a href="#" class="btn btn-success">Comentarios <span class="badge">20</span></a>
                    </div>
                </article>
                <article class="post clearfix">
                    <a href="#" class="thumb pull-left">
                        <img class="img-thumbnail" src="CAPA_PRESENTACION/images/img2.jpg" alt="">
                    </a>
                    <h2 class="post-tittle">
                        <a href="#">Inicia proyectos HTML5 mas rápido con Initializar</a>
                    </h2>
                    <p><span class="post-fecha">26 de enero del 2015</span> por <span class="post-autor"><a href="#">Manuel Guarniz</a></span></p>
                    <p class="post-contenido text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                    <div class="contenedor-botones">
                        <a href="#" class="btn btn-primary">Leer más</a>
                        <a href="#" class="btn btn-success">Comentarios <span class="badge">20</span></a>
                    </div>
                </article>
                <article class="post clearfix">
                    <a href="#" class="thumb pull-left">
                        <img class="img-thumbnail" src="CAPA_PRESENTACION/images/img3.jpg" alt="">
                    </a>
                    <h2 class="post-tittle">
                        <a href="#">Inicia proyectos HTML5 mas rápido con Initializar</a>
                    </h2>
                    <p><span class="post-fecha">26 de enero del 2015</span> por <span class="post-autor"><a href="#">Manuel Guarniz</a></span></p>
                    <p class="post-contenido text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                    <div class="contenedor-botones">
                        <a href="#" class="btn btn-primary">Leer más</a>
                        <a href="#" class="btn btn-success">Comentarios <span class="badge">20</span></a>
                    </div>
                </article>
            </section>
            <aside class="col-md-3 hidden-xs hidden-sm">
                <h4>Categorias</h4>
                <div class="list-group">
                    <a href="#" class="list-group-item active">Diseño Web</a>
                    <a href="#" class="list-group-item">CSS</a>
                    <a href="#" class="list-group-item">Cursos</a>
                    <a href="#" class="list-group-item">Dessarrollo Web</a>
                    <a href="#" class="list-group-item">Elementos Web</a>
                    <a href="#" class="list-group-item">JQuery</a>
                    <a href="#" class="list-group-item">Recursos y Herramientas</a>
                </div>
                <h4>Articulos Recientes</h4>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Iniciar Proyectos mas rapido con Bootstrap</h4>
                    <p class="list-group-item-text">Aprende como crear páginas web muy rapido con Bootstrap</p>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Iniciar Proyectos mas rapido con Bootstrap</h4>
                    <p class="list-group-item-text">Aprende como crear páginas web muy rapido con Bootstrap</p>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Iniciar Proyectos mas rapido con Bootstrap</h4>
                    <p class="list-group-item-text">Aprende como crear páginas web muy rapido con Bootstrap</p>
                </a>
            </aside>
        </div>
    </section>
    <footer class="container">
        <div class="row">
            <div class="col-xs-6">
                <p>Manuel Guarniz - OneSevenDevelopment</p>
            </div>
            <div class="col-xs-6">
                <ul class="list-inline text-right">
                    <li><a href="#">Inicio</a></li>
                    <li><a href="#">Cursos</a></li>
                    <li><a href="#">Contactos</a></li>
                </ul>
            </div>
        </div>
        <!-- Probando zen coding
        div.col-xs-6>ul.list-inline.text-right>li*3>a-->
    </footer>
    <script src="js/jquery-2.1.4.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/controaldor.js"></script>
	<!-- dkfughsdfkjyudgsdkfyud
	-->
</body>
</html>
